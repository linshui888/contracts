## BungeeCord Data Sync

There is 2 SQL tables for contracts and for playerData.
When a server is empty nothing is stored in the RAM, when the first player joins all
the contract data is loaded in the RAM. The playerData is loaded on the fly when needed
and cached for 10 min. Whenever an action occurs regarding the playerData or the contracts
it is synced with all the servers by saving the data in the SQL database and then notifying
all the servers to update their data through plugin messages.
The trick regarding is_saved is not necessary as the data in the SQL database is always up to date.
(Which is not the case for MMOCore).